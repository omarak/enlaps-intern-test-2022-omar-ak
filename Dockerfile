FROM python:3.8
COPY requirements.txt .
ADD main.py . 
ARG IMAGE_FOLDER
RUN pip install --no-cache-dir --upgrade pip && \
    pip install --no-cache-dir -r requirements.txt
COPY . .
CMD ["python3","main.py",$IMAGE_FOLDER]
