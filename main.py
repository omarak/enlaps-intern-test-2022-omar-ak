from argparse import ArgumentParser
import sys
import os
import tensorflow as tf
import numpy as np
import PIL
import PIL.Image
from scipy.interpolate import interp1d
import matplotlib.image as img

class_names = ['daisy', 'dandelion', 'roses', 'sunflowers', 'tulips']

def parse_args():
    parser = ArgumentParser()
    parser.add_argument("input_folder", type=str, help="A folder with images to analyze.")
    args = parser.parse_args()
    
    return args


if __name__ == "__main__":
    #Call parsing function
    cli_args = parse_args()
    
    #List all available jpgs in the input folder
    filelist=os.listdir(cli_args.input_folder)
    for fichier in filelist[:]:
        if not(fichier.endswith(".jpg")):
            filelist.remove(fichier)

    #Print which folder we are reading
    print(f"Analyzing folder : {cli_args.input_folder}", file=sys.stderr)
    
    #Allocate tensors and get input output details of the trained network
    interpreter = tf.lite.Interpreter(model_path="model.tflite")
    interpreter.allocate_tensors()
    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()
    
    #Changegirectory into folder with jpgs
    os.chdir(cli_args.input_folder)
    
    #for each jpg in this list
    for i in filelist:
        
        #Read-Reshape[180 180]-Add dimension for batchsize of 1 equivalent to unsqueeze in pytorch
        image = img.imread(i)
        image=image[int(image.shape[0]/2)-90:int(image.shape[0]/2)+90,int(image.shape[1]/2)-90:int(image.shape[1]/2)+90]
        im=np.expand_dims(image,0)
        
        #Change dtype-Set Tensor- Call classifier 
        im=im.astype('float32')
        interpreter.set_tensor(input_details[0]['index'], im)
        interpreter.invoke()
        
        #Get ouput data
        output_data = interpreter.get_tensor(output_details[0]['index'])
        
        #Needed since ouput format is [[]]
        out=max(output_data)
        
        #Normalizing the data into a probabilistic style 
        out=out-min(out)
        s=np.sum(out)
        out=out/s
        
        #Output the result of the classifier
        print({i:{"score":max(out), "class":class_names[np.argmax(out)]}})
